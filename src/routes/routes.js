import Home from 'views/Home';
import Login from 'views/Login';
import { LogoutRequired, LoginRequired } from 'components/RouteWrappers';
import { Sidebar } from 'layouts';
import App from 'app/App';
import endpoints from './endpoints';

export default [
  {
    component: App,
    routes: [
      {
        path: endpoints.login,
        exact: true,
        component: LogoutRequired,
        routes: [
          {
            component: Login,
            exact: true
          }
        ]
      },
      {
        path: endpoints.dashboard,
        component: LoginRequired,
        routes: [
          {
            path: endpoints.dashboard,
            component: Sidebar,
            exact: true,
            routes: [
              {
                component: Home
              }
            ]
          },
        ]
      }
    ]
  }
];
