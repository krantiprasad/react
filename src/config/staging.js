const API = {
  BASE_URL: '/api/v1',
};

const DEV_TOOLS = {
  logError: true
};

module.exports = {
  API,
  DEV_TOOLS
};
