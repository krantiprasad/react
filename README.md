# mayaedu

mayaedu

## Getting Started

Steps to run `mayaedu` application

## Prerequisites

`npm`: npm version: 6.13.4

`node`: node version: v12.14.1

## Installation

git clone `clone url`

cd mayaedu-frontend

`npm install`
(It will install required dependencies and link modules internally.)

## Folder Structure

After creation, your project should look like this:

```
mayaedu-frontend/
  README.md
  node_modules/
  package-lock.json
  package.json
  src/
    api/
    assets/
      -fonts/
      -styles/
      -img/
    components/
    config/
    libs/
    redux/
    views/
    index.jsx/
    index.html
```

## Available scripts

To start application in development environment.

`npm start`
Runs the app in the development mode. Open `http://localhost:8888` to view it in the browser.

`npm run build-production`
Builds the app for production to the `build` folder.

`npm run build-staging`
Builds the app for staging to the `build` folder.
